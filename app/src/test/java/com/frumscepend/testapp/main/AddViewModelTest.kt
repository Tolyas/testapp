package com.frumscepend.testapp.main

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.data.source.ProductDataSource
import com.frumscepend.testapp.data.source.ProductRepository
import com.frumscepend.testapp.main.viewmodel.AddViewModel
import com.frumscepend.testapp.util.any
import com.frumscepend.testapp.util.capture
import com.frumscepend.testapp.util.eq
import com.google.common.collect.Lists
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.verify

class AddViewModelTest {
    @Mock private lateinit var productRepository: ProductRepository
    @Mock private lateinit var context: Application
    @Captor private lateinit var insertProductsCallbackCaptor: ArgumentCaptor<ProductDataSource.InsertProductsCallback>
    private lateinit var addViewModel: AddViewModel
    private lateinit var products: List<Product>

    @Before fun setupAddViewModel(){
        MockitoAnnotations.initMocks(this)

        setupContext()

        // Get a reference to the class under test
        addViewModel = AddViewModel(context, productRepository)

        products = Lists.newArrayList(
                Product("Test 1", 100.0, 10),
                Product("Test 2", 300.2, 5),
                Product("Test 3", 50.0, 0)
        )
    }

    private fun setupContext() {
        Mockito.`when`<Context>(context.applicationContext).thenReturn(context)
        Mockito.`when`(context.resources).thenReturn(Mockito.mock(Resources::class.java))
    }

    @Test fun startTest(){
        with(addViewModel){
            start(products[0])
            // Checking added data
            assertTrue(product.get() != null)
            assertTrue(product.get()?.name == products[0].name)
        }
    }

    @Test fun saveProductTest(){
        with(addViewModel){
            callback = Mockito.mock(ProductDataSource.InsertProductsCallback::class.java)
            product.set(products[0])
            onSaveClick(products[0].name, products[0].cost.toString(), products[0].count.toString())

            verify<ProductRepository>(productRepository).saveProduct(eq(products[0]), capture(insertProductsCallbackCaptor))
            insertProductsCallbackCaptor.value.onReady()
        }
    }

    @Test fun createProductTest(){
        with(addViewModel){
            callback = Mockito.mock(ProductDataSource.InsertProductsCallback::class.java)
            onSaveClick(products[0].name, products[0].cost.toString(), products[0].count.toString())

            verify<ProductRepository>(productRepository).createProduct(any(), capture(insertProductsCallbackCaptor))
            insertProductsCallbackCaptor.value.onReady()
        }
    }
}
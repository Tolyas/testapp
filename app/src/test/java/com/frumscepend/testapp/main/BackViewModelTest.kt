package com.frumscepend.testapp.main

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.data.source.ProductDataSource
import com.frumscepend.testapp.data.source.ProductRepository
import com.frumscepend.testapp.main.viewmodel.BackViewModel
import com.frumscepend.testapp.util.capture
import com.frumscepend.testapp.util.mock
import com.google.common.collect.Lists
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.verify

class BackViewModelTest {
    @Mock private lateinit var productRepository: ProductRepository
    @Mock private lateinit var context: Application
    @Captor private lateinit var loadProductsCallbackCaptor: ArgumentCaptor<ProductDataSource.LoadProductsCallback>
    private lateinit var backViewModel: BackViewModel
    private lateinit var products: List<Product>

    @Before fun setupAddViewModel(){
        MockitoAnnotations.initMocks(this)

        setupContext()

        // Get a reference to the class under test
        backViewModel = BackViewModel(context, productRepository)

        products = Lists.newArrayList(
                Product("Test 1", 100.0, 10),
                Product("Test 2", 300.2, 5),
                Product("Test 3", 50.0, 0)
        )
    }

    private fun setupContext() {
        Mockito.`when`<Context>(context.applicationContext).thenReturn(context)
        Mockito.`when`(context.resources).thenReturn(Mockito.mock(Resources::class.java))
    }


    @Test fun startSuccessTest(){
        with(backViewModel){
            listAdapter = mock()
            start()
            verify<ProductRepository>(productRepository).getProducts(capture(loadProductsCallbackCaptor))

            assertTrue(dataLoading.get())
            loadProductsCallbackCaptor.value.onProductsLoaded(products)
            assertFalse(dataLoading.get())
            assertFalse(productsList.isEmpty())
            assertFalse(error.get())
            assertTrue(productsList.size == 3)
        }
    }

    @Test fun startFailTest(){
        with(backViewModel){
            listAdapter = mock()
            start()
            verify<ProductRepository>(productRepository).getProducts(capture(loadProductsCallbackCaptor))

            assertTrue(dataLoading.get())
            loadProductsCallbackCaptor.value.onDataNotAvailable()
            assertTrue(productsList.isEmpty())
            assertTrue(error.get())
        }
    }
}
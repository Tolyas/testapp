package com.frumscepend.testapp.main

import android.app.Application
import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.res.Resources
import com.frumscepend.testapp.R
import com.frumscepend.testapp.TestingUtils
import com.frumscepend.testapp.util.capture
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.data.source.ProductDataSource
import com.frumscepend.testapp.data.source.ProductRepository
import com.frumscepend.testapp.main.viewmodel.AddViewModel
import com.frumscepend.testapp.main.viewmodel.StoreItemViewModel
import com.frumscepend.testapp.main.viewmodel.StoreViewModel
import com.frumscepend.testapp.util.eq
import com.frumscepend.testapp.util.mock
import com.google.common.collect.Lists
import junit.framework.Assert.assertTrue
import junit.framework.Assert.assertFalse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.verify

class StoreItemViewModelTest {
    // Execute tasks synchronously
    @get:Rule var instantExecutorRule = InstantTaskExecutorRule()
    @Mock private lateinit var productRepository: ProductRepository
    @Mock private lateinit var context: Application
    @Captor private lateinit var productCallbackCaptor: ArgumentCaptor<ProductDataSource.ProductCallback>
    private lateinit var storeItemViewModel: StoreItemViewModel
    private lateinit var productTest: Product

    @Before fun setupAddViewModel(){
        MockitoAnnotations.initMocks(this)

        setupContext()

        // Get a reference to the class under test
        storeItemViewModel = StoreItemViewModel(context, productRepository)

        productTest = Product("Test 1", 100.0, 10)

        // Remove observers from snackbar message
        storeItemViewModel.message.removeObservers(TestingUtils.TESTING_OBSERVER)
    }

    private fun setupContext() {
        Mockito.`when`<Context>(context.applicationContext).thenReturn(context)
        Mockito.`when`(context.resources).thenReturn(Mockito.mock(Resources::class.java))
    }

    @Test fun startSuccessTest(){
        with(storeItemViewModel){
            start(productTest)
            assertTrue(product.get() != null)
            assertTrue(product.get()?.id == productTest.id)
        }
    }

    @Test fun buySuccessTest(){
        with(storeItemViewModel){
            product.set(productTest)
            onBuyClick()
            verify<ProductRepository>(productRepository).buyProduct(eq(productTest), capture(productCallbackCaptor))
            productCallbackCaptor.value.onSuccess(productTest)
        }
    }

    @Test fun buyFailTest(){
        with(storeItemViewModel){
            // Declare observer
            val observer = mock<Observer<Int>>()
            message.observe(TestingUtils.TESTING_OBSERVER, observer)
            product.set(productTest)
            onBuyClick()
            verify<ProductRepository>(productRepository).buyProduct(eq(productTest), capture(productCallbackCaptor))
            productCallbackCaptor.value.onFail()

            // Then the event is triggered
            verify<Observer<Int>>(observer).onChanged(R.string.invalid_buy)
        }
    }
}
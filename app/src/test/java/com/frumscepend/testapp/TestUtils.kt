
package com.frumscepend.testapp

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LifecycleRegistry


object TestingUtils {
    val TESTING_OBSERVER: LifecycleOwner = object : LifecycleOwner {
        // Creates a LifecycleRegistry in state 'resumed'.
        private val registry = LifecycleRegistry(this).apply {
            handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
            handleLifecycleEvent(Lifecycle.Event.ON_START)
            handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
        }

        override fun getLifecycle() = registry
    }
}

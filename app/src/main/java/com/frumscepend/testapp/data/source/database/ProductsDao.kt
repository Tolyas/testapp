package com.frumscepend.testapp.data.source.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.frumscepend.testapp.data.Product

/**
 * Data Access Object for products
 */

@Dao
interface ProductsDao {

    /**
     * Getting all existing products
     *
     * @return all products
     */
    @Query("SELECT * FROM products")
    fun getAllProducts(): List<Product>

    /**
     * Getting all existing products
     *
     * @return all products
     */
    @Query("SELECT * FROM products WHERE id=:id LIMIT 1")
    fun getProductById(id: String): Product

    /**
     * Getting all products with count > 0
     *
     * @return All available products
     */
    @Query("SELECT * FROM products WHERE count > 0")
    fun getAllActiveProducts(): List<Product>

    /**
     * Creating new product
     *
     * @param product the product to be inserted
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProducts(product: Product)

    /**
     * Creating set of new products
     *
     * @param products the products list to be inserted
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllProducts(products: List<Product>)

    /**
     * Updating product
     *
     * @param count the count of products
     * @param cost the cost of the product
     * @param name the name of the product
     */
    @Query("UPDATE products SET count = :count, cost = :cost, name = :name WHERE id = :id")
    fun updateProduct(count: Int, cost: Double, name: String, id: String): Int

    /**
     * Deleting product from the database
     */
    @Query("DELETE FROM products WHERE id = :id")
    fun deleteProduct(id: String): Int
}
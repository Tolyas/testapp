package com.frumscepend.testapp.main.activity

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.frumscepend.testapp.R
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.data.source.ProductDataSource
import com.frumscepend.testapp.databinding.ActivityAddProductBinding
import com.frumscepend.testapp.events.DataUpdateEvent
import com.frumscepend.testapp.main.viewmodel.AddViewModel
import com.frumscepend.testapp.utils.obtainViewModel
import org.greenrobot.eventbus.EventBus

class AddProductActivity : AppCompatActivity() {

    private lateinit var activityAddProductBinding: ActivityAddProductBinding

    companion object {
        fun getNewIntent(context: Context, product: Product? = null): Intent {
            val intent = Intent(context, AddProductActivity::class.java)
            product?.let {
                intent.putExtra("product", product)
            }
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityAddProductBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_product)
        val product: Product? = intent.getSerializableExtra("product") as Product?
        activityAddProductBinding.viewmodel = obtainViewModel(AddViewModel::class.java)
        activityAddProductBinding.viewmodel?.let { viewmodel ->
            product?.let { prod ->
                viewmodel.product.set(prod)
            }
            viewmodel.callback = object : ProductDataSource.InsertProductsCallback {
                override fun onReady() {
                    /**
                     * Notify everyone about data update
                     */
                    EventBus.getDefault().post(DataUpdateEvent())
                    finish()
                }
            }
        }
    }
}

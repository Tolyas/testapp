package com.frumscepend.testapp.main.databindingadapter

import android.databinding.BindingMethod
import android.databinding.BindingMethods
import android.support.v4.view.ViewPager
import android.databinding.adapters.ListenerUtil
import android.databinding.InverseBindingListener
import android.databinding.BindingAdapter
import android.databinding.InverseBindingAdapter
import com.frumscepend.testapp.R


@BindingMethods(
        BindingMethod(type = ViewPager::class, attribute = "app:offscreenPageLimit", method = "setOffscreenPageLimit"),
        BindingMethod(type = ViewPager::class, attribute = "app:adapter", method = "setAdapter"),
        BindingMethod(type = ViewPager::class, attribute = "app:currentPage", method = "setCurrentItem")
)
class ViewPagerDataBindingAdapter private constructor() {

    init {
        throw UnsupportedOperationException()
    }
}
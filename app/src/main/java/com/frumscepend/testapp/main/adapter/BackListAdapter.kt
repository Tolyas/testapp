package com.frumscepend.testapp.main.adapter

import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.databinding.ItemBackListBinding
import com.frumscepend.testapp.main.viewmodel.BackViewModel


class BackListAdapter: RecyclerView.Adapter<BackListAdapter.MyViewHolder>(){
    var onEditListener: BackViewModel.OnEditListener? = null
    var products: ObservableList<Product> = ObservableArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class MyViewHolder(private val binding: ItemBackListBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Product) {
            binding.product = item
            binding.listener = onEditListener
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemBackListBinding.inflate(layoutInflater, parent, false)
        return MyViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(products[position])
    }

    override fun getItemCount(): Int {
        return products.size
    }

}
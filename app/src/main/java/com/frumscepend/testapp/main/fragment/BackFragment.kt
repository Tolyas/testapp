package com.frumscepend.testapp.main.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.databinding.FragmentBackBinding
import com.frumscepend.testapp.events.DataUpdateEvent
import com.frumscepend.testapp.main.activity.AddProductActivity
import com.frumscepend.testapp.main.activity.MainActivity
import com.frumscepend.testapp.main.viewmodel.BackViewModel
import com.frumscepend.testapp.utils.obtainViewModel
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode




class BackFragment: Fragment(){

    private lateinit var fragmentBackBinding: FragmentBackBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentBackBinding = FragmentBackBinding.inflate(inflater, container, false).apply {
            viewmodel = (activity as MainActivity).obtainViewModel(BackViewModel::class.java)
        }
        fragmentBackBinding.viewmodel?.let {
            it.onAddListener = object : BackViewModel.OnAddListener {
                override fun onAdd() {
                    activity?.startActivity(AddProductActivity.getNewIntent(context = context!!))
                }
            }
            it.onEditListener = object : BackViewModel.OnEditListener {
                override fun onEdit(product: Product) {
                    activity?.startActivity(AddProductActivity.getNewIntent(context = context!!, product = product))
                }
            }
        }
        return fragmentBackBinding.root
    }

    /**
     * Updating data by request
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: DataUpdateEvent) {
        fragmentBackBinding.viewmodel?.start()
    }

    override fun onResume() {
        super.onResume()
        fragmentBackBinding.viewmodel?.start()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    companion object {
        fun newInstance() = BackFragment()
    }
}
package com.frumscepend.testapp.main.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableList
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.data.source.ProductDataSource
import com.frumscepend.testapp.main.adapter.BackListAdapter


class BackViewModel (
        private val context: Application,
        private val productRepository: ProductDataSource
): AndroidViewModel(context) {

    val dataLoading = ObservableBoolean(false)
    val error = ObservableBoolean(false)
    val empty = ObservableBoolean()
    val productsList: ObservableList<Product> = ObservableArrayList()
    var listAdapter = BackListAdapter()
    var onAddListener: OnAddListener? = null
    var onEditListener: OnEditListener? = null

    fun start() {
        loadProducts()
    }

    fun loadProducts() {
        dataLoading.set(true)
        productRepository.getProducts(object : ProductDataSource.LoadProductsCallback {

            override fun onProductsLoaded(products: List<Product>) {
                error.set(false)
                computeProducts(products)
            }

            override fun onDataNotAvailable() {
                error.set(true)
            }

        })
    }

    /**
     * called when data is ready
     */
    fun computeProducts(products: List<Product>) {
        productsList.clear()
        productsList.addAll(products)
        listAdapter.products = productsList
        listAdapter.onEditListener = onEditListener
        empty.set(productsList.isEmpty())
        dataLoading.set(false)
    }

    interface OnAddListener{
        fun onAdd()
    }

    interface OnEditListener{
        fun onEdit(product: Product)
    }
}
package com.frumscepend.testapp.main.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.main.fragment.StoreItemFragment
import com.frumscepend.testapp.main.viewmodel.StoreViewModel

class StoreAdapter (
        private val viewModel: StoreViewModel,
        fm: FragmentManager
): FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        val storeItemFragment = StoreItemFragment.newInstance()
        storeItemFragment.product = viewModel.productsList[position]
        return storeItemFragment
    }

    override fun getCount(): Int {
        return viewModel.productsList.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }
}
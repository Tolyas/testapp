package com.frumscepend.testapp

import android.content.Context

import com.frumscepend.testapp.data.source.ProductRepository
import com.frumscepend.testapp.data.source.database.MyDatabase
import com.frumscepend.testapp.data.source.database.ProductDatabaseDataSource
import com.frumscepend.testapp.data.source.csv.ProductCSVDataSource
import com.frumscepend.testapp.utils.AppExecutors

/**
 * Enables injection of [ProductDatabaseDataSource] and [ProductCSVDataSource] or mock it for testing
 */
object Injection {

    fun provideTasksRepository(context: Context): ProductRepository {
        val database = MyDatabase.getInstance(context)
        return ProductRepository.getInstance(ProductDatabaseDataSource.getInstance(AppExecutors(), database.taskDao()),
                ProductCSVDataSource.getInstance(AppExecutors(), context))
    }
}

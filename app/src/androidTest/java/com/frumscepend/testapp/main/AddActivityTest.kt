package com.frumscepend.testapp.main

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.filters.LargeTest
import android.support.test.runner.AndroidJUnit4
import com.frumscepend.testapp.R
import com.frumscepend.testapp.main.activity.AddProductActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class) @LargeTest
class AddActivityTest {
    @get:Rule var addActivityTestRule = IntentsTestRule(AddProductActivity::class.java)


    @Test fun addActivityTest(){
        onView(withId(R.id.editTextName)).perform(clearText(), typeText("Test 01"))
        onView(withId(R.id.editTextCostValue)).perform(clearText(), typeText("100.01"))
        onView(withId(R.id.editTextAmountValue)).perform(clearText(), typeText("10"))
        onView(withId(R.id.buttonSave)).perform(click())
    }
}